/*****************************************************************************
 * rsarray (test) - Resizable array (test).
 *
 * Fri Dec 30 19:37:58 GMT 2022
 *
 * Copyright (C) 2022-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of rsarray.
 *
 * rsarray is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * rsarray is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with rsarray; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2022-12-30 Extracted from csstring_test.c.
 * 2022-12-31 Add rsarray_element_insert().
 * 2023-01-01 Add rsarray_element_delete().
 *            Add limits (low and high).
 * 2023-01-04 Replace tabs with spaces.
 * 2023-01-07 Automatically initialise the array with a poison value (or 0).
 * 2023-01-17 Allow initialisation with a size of 0.
 * 2023-01-18 Allow array to shrink to a size of 0.
 * 2023-01-21 Allow a lower size limit of 0.
 * 2023-01-21 Append and prepend.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <malloc.h>
#include <string.h>

#include "rsarray.h"
#include "rsarray_private.h"

#include "cutl.h"
#include "mal.h"
#include "timber.h"

void test_rsarray_create(void);
void test_rsarray_create_poisoned(void);
void test_rsarray_assign(void);
void test_rsarray_element_insert(void);
void test_rsarray_element_delete(void);
void test_rsarray_limit_low(void);
void test_rsarray_limit_high(void);
void test_rsarray_limit_low_too_low(void);
void test_rsarray_limit_low_too_high(void);
void test_rsarray_limit_high_too_low(void);
void test_rsarray_limit_high_zero(void);
void test_rsarray_create_zero(void);
void test_rsarray_zero_insert(void);
void test_rsarray_element_append(void);
void test_rsarray_element_prepend(void);

cutl_test_case_t tests[] =
{
    { "rsarray_create",                 test_rsarray_create              },
    { "rsarray_create_poisoned",        test_rsarray_create_poisoned     },
    { "rsarray_assign",                 test_rsarray_assign              },
    { "rsarray_element_insert",         test_rsarray_element_insert      },
    { "rsarray_element_delete",         test_rsarray_element_delete      },
    { "rsarray_limit_low",              test_rsarray_limit_low           },
    { "rsarray_limit_high",             test_rsarray_limit_high          },
    { "rsarray_limit_low_too_low",      test_rsarray_limit_low_too_low   },
    { "rsarray_limit_low_too_high",     test_rsarray_limit_low_too_high  },
    { "rsarray_limit_high_too_low",     test_rsarray_limit_high_too_low  },
    { "rsarray_limit_high_zero",        test_rsarray_limit_high_zero     },
    { "rsarray_create_zero",            test_rsarray_create_zero         },
    { "rsarray_zero_insert",            test_rsarray_zero_insert         },
    { "rsarray_element_append",         test_rsarray_element_append      },
    { "rsarray_element_prepend",        test_rsarray_element_prepend     },
};

#define NO_OF_OBJECTS   32

void rsarray_test_case_setup(void)
{
    MAL_OPEN(NO_OF_OBJECTS);
}

void rsarray_test_case_teardown(void)
{
    int32_t nspare;

    nspare = MAL_CLOSE();
    if (nspare != 0)
    {
        printf("Number of unreclaimed pointers: %d\n", nspare);
    } 
}

int main(int argc, char *argv[])
{
    cutl_set_test_lib("rsarray_test", tests, NO_OF_ELEMENTS(tests));
    cutl_set_cutl_test_setup(rsarray_test_case_setup);
    cutl_set_cutl_test_teardown(rsarray_test_case_teardown);
    cutl_get_options(argc, argv);
    cutl_run_tests();

    return 0;
}

#define FIVE_ELEMENTS   5
void test_rsarray_create(void)
{
    rsarray_t *array = NULL;
    int32_t index;
    void *element;

    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size",       FIVE_ELEMENTS,              rsarray_size_get(array));
    TEST_SIZE_T_EQUAL("limit_low",  RSARRAY_LIMIT_LOW_HARD,     rsarray_limit_low_get(array));
    TEST_SIZE_T_EQUAL("limit_high", RSARRAY_LIMIT_HIGH_DEFAULT, rsarray_limit_high_get(array));

    TEST_PTR_EQUAL("poison", (void *)0x0, rsarray_poison_get(array));

    for (index = 0; index < rsarray_size_get(array); index++)
    {
        TEST_INT32_EQUAL("rsarray_element_get", 0, rsarray_element_get(array, index, &element));
        TEST_PTR_EQUAL("content", (void *)0x0, element);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

void test_rsarray_create_poisoned(void)
{
    rsarray_t *array = NULL;
    int32_t index;
    void *element;

    array = rsarray_create_poisoned(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size",       FIVE_ELEMENTS,              rsarray_size_get(array));
    TEST_SIZE_T_EQUAL("limit_low",  RSARRAY_LIMIT_LOW_HARD,     rsarray_limit_low_get(array));
    TEST_SIZE_T_EQUAL("limit_high", RSARRAY_LIMIT_HIGH_DEFAULT, rsarray_limit_high_get(array));

#if 0
    TEST_PTR_EQUAL("poison", (void *)0xbaddcafedeadbeef, rsarray_poison_get(array));
#else
    TEST_PTR_EQUAL("poison", (void *)0xbaddbeefcafebadd, rsarray_poison_get(array));
#endif

    for (index = 0; index < rsarray_size_get(array); index++)
    {
        TEST_INT32_EQUAL("rsarray_element_get", 0, rsarray_element_get(array, index, &element));
#if 0
        TEST_PTR_EQUAL("content", (void *)0xbaddcafedeadbeef, element);
#else
        TEST_PTR_EQUAL("content", (void *)0xbaddbeefcafebadd, element);
#endif
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

void test_rsarray_assign(void)
{
    rsarray_t *array = NULL;
    int32_t numbers[FIVE_ELEMENTS] = { 1, 2, 3, 4, 5 };
    int32_t index;
    int32_t retcode;
    void *ptr;

    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size", FIVE_ELEMENTS, rsarray_size_get(array));

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    for (index = 0; index < NO_OF_ELEMENTS(array); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", numbers[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

void test_rsarray_element_insert(void)
{
    rsarray_t *array = NULL;
    int32_t numbers[FIVE_ELEMENTS] = { 1, 2, 3, 4, 5 };
    int32_t index;
    int32_t retcode;
    void *ptr;
    int32_t expected_0[FIVE_ELEMENTS] = { 1, 2, 3, 4, 5 };
    int32_t extra = 99;
    int32_t expected_1[FIVE_ELEMENTS + 1] = { 99, 1, 2, 3, 4, 5 };
    int32_t expected_2[FIVE_ELEMENTS + 1] = { 1, 99, 2, 3, 4, 5 };
    int32_t expected_3[FIVE_ELEMENTS + 1] = { 1, 2, 99, 3, 4, 5 };
    int32_t expected_4[FIVE_ELEMENTS + 1] = { 1, 2, 3, 99, 4, 5 };
    int32_t expected_5[FIVE_ELEMENTS + 1] = { 1, 2, 3, 4, 99, 5 };
    int32_t expected_6[FIVE_ELEMENTS + 1] = { 1, 2, 3, 4, 5, 99 };

    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size", FIVE_ELEMENTS, rsarray_size_get(array));

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_0[index], *(int32_t *)ptr);
    }

    /*
     * Insert at position 0
     */
    retcode = rsarray_element_insert(array, 0, &extra);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_1); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_1[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);

    /*
     * Insert at position 1
     */
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    retcode = rsarray_element_insert(array, 1, &extra);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_2); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_2[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);

    /*
     * Insert at position 2
     */
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    retcode = rsarray_element_insert(array, 2, &extra);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_3); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_3[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);

    /*
     * Insert at position 3
     */
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    retcode = rsarray_element_insert(array, 3, &extra);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_4); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_4[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);

    /*
     * Insert at position 4
     */
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    retcode = rsarray_element_insert(array, 4, &extra);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_5); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_5[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);

    /*
     * Insert at position 5
     */
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    retcode = rsarray_element_insert(array, 5, &extra);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_6); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_6[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);

    /*
     * Out of bounds.
     */
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    retcode = rsarray_element_insert(array, -1, &extra);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    retcode = rsarray_element_insert(array, 6, &extra);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_0); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_0[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

void test_rsarray_element_delete(void)
{
    rsarray_t *array = NULL;
    int32_t numbers[FIVE_ELEMENTS] = { 1, 2, 3, 4, 5 };
    int32_t index;
    int32_t retcode;
    void *ptr;
    int32_t expected_0[FIVE_ELEMENTS] = { 1, 2, 3, 4, 5 };
    int32_t expected_1[] = { 2, 3, 4, 5 };
    int32_t expected_2[] = { 1, 3, 4, 5 };
    int32_t expected_3[] = { 1, 2, 4, 5 };
    int32_t expected_4[] = { 1, 2, 3, 5 };
    int32_t expected_5[] = { 1, 2, 3, 4 };

    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size", FIVE_ELEMENTS, rsarray_size_get(array));

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_0[index], *(int32_t *)ptr);
    }

    /*
     * Delete at position 0
     */
    retcode = rsarray_element_delete(array, 0);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_1); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_1[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);

    /*
     * Delete at position 1
     */
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    retcode = rsarray_element_delete(array, 1);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_2); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_2[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);

    /*
     * Delete at position 2
     */
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    retcode = rsarray_element_delete(array, 2);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_3); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_3[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);

    /*
     * Delete at position 3
     */
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    retcode = rsarray_element_delete(array, 3);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_4); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_4[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);

    /*
     * Delete at position 4
     */
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    retcode = rsarray_element_delete(array, 4);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_5); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_5[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);

    /*
     * Out of bounds.
     */
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    retcode = rsarray_element_delete(array, -1);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    retcode = rsarray_element_delete(array, 6);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_0); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_0[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

void test_rsarray_limit_low(void)
{
    rsarray_t *array = NULL;
    int32_t numbers[FIVE_ELEMENTS] = { 1, 2, 3, 4, 5 };
    int32_t index;
    int32_t retcode;
    void *ptr;
    int32_t expected_0[FIVE_ELEMENTS] = { 1, 2, 3, 4, 5 };
    int32_t expected_1[] = { 2, 3, 4, 5 };
    int32_t expected_2[] = { 3, 4, 5 };
    int32_t expected_3[] = { 4, 5 };
    int32_t expected_4[] = { 5 };

    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size", FIVE_ELEMENTS, rsarray_size_get(array));

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_0[index], *(int32_t *)ptr);
    }

    /*
     * Delete at position 0 until 1 element left
     */
    retcode = rsarray_element_delete(array, 0);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_1); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_1[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_delete(array, 0);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_2); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_2[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_delete(array, 0);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_3); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_3[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_delete(array, 0);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_4); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_4[index], *(int32_t *)ptr);
    }

    /*
     * Show that we can delete the last element.
     */
    retcode = rsarray_element_delete(array, 0);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    TEST_SIZE_T_EQUAL("size", 0, rsarray_size_get(array));
    TEST_PTR_NULL("data", rsarray_data_get(array));

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

#define THREE_ELEMENTS  3
void test_rsarray_limit_high(void)
{
    rsarray_t *array = NULL;
    int32_t numbers[THREE_ELEMENTS] = { 3, 2, 1 };
    int32_t extras[] = { 4, 5, 6 };
    int32_t index;
    int32_t retcode;
    void *ptr;
    int32_t expected_0[THREE_ELEMENTS] = { 3, 2, 1 };
    int32_t expected_1[] = { 4, 3, 2, 1 };
    int32_t expected_2[] = { 5, 4, 3, 2, 1 };

    array = rsarray_create(THREE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size", THREE_ELEMENTS, rsarray_size_get(array));

    retcode = rsarray_limit_high_set(array, FIVE_ELEMENTS);
    TEST_INT32_EQUAL("rsarray_limit_high_set", 0, retcode);
    TEST_SIZE_T_EQUAL("limit_high", FIVE_ELEMENTS, rsarray_limit_high_get(array));

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_set(array, index, &numbers[index]);
        TEST_INT32_EQUAL("retcode", 0, retcode);
    }

    for (index = 0; index < NO_OF_ELEMENTS(numbers); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_0[index], *(int32_t *)ptr);
    }

    /*
     * Insert at position 0 until 5 elements in array.
     */
    retcode = rsarray_element_insert(array, 0, &extras[0]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_1); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_1[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_insert(array, 0, &extras[1]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_2); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_2[index], *(int32_t *)ptr);
    }

    /*
     * Show that we can't insert another element.
     */
    retcode = rsarray_element_insert(array, 0, &extras[2]);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_2); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_2[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

/*
 * No "too low" limit any more.
 * Size (length) of 0 is now permitted.
 */
void test_rsarray_limit_low_too_low(void)
{
    rsarray_t *array = NULL;
    int32_t retcode;
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size",       FIVE_ELEMENTS,              rsarray_size_get(array));
    TEST_SIZE_T_EQUAL("limit_low",  RSARRAY_LIMIT_LOW_HARD,     rsarray_limit_low_get(array));
    TEST_SIZE_T_EQUAL("limit_high", RSARRAY_LIMIT_HIGH_DEFAULT, rsarray_limit_high_get(array));

    retcode = rsarray_limit_low_set(array, 0);
    TEST_INT32_EQUAL("limit_low", 0, retcode);

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

void test_rsarray_limit_low_too_high(void)
{
    rsarray_t *array = NULL;
    int32_t retcode;
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size",       FIVE_ELEMENTS,              rsarray_size_get(array));
    TEST_SIZE_T_EQUAL("limit_low",  RSARRAY_LIMIT_LOW_HARD,     rsarray_limit_low_get(array));
    TEST_SIZE_T_EQUAL("limit_high", RSARRAY_LIMIT_HIGH_DEFAULT, rsarray_limit_high_get(array));

    retcode = rsarray_limit_low_set(array, rsarray_limit_high_get(array) + 1);
    TEST_INT32_EQUAL("limit_low", -1, retcode);

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

void test_rsarray_limit_high_too_low(void)
{
    rsarray_t *array = NULL;
    int32_t retcode;
    array = rsarray_create(FIVE_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size",       FIVE_ELEMENTS,              rsarray_size_get(array));
    TEST_SIZE_T_EQUAL("limit_low",  RSARRAY_LIMIT_LOW_HARD,     rsarray_limit_low_get(array));
    TEST_SIZE_T_EQUAL("limit_high", RSARRAY_LIMIT_HIGH_DEFAULT, rsarray_limit_high_get(array));

    retcode = rsarray_limit_high_set(array, rsarray_size_get(array) - 1);
    TEST_INT32_EQUAL("limit_high", -1, retcode);

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

/*
 * Disallow a high limit of 0.
 * Would such a limit ever make sense?
 */
#define NO_ELEMENTS 0
void test_rsarray_limit_high_zero(void)
{
    rsarray_t *array = NULL;
    int32_t retcode;
    array = rsarray_create(NO_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size",       0,                          rsarray_size_get(array));
    TEST_SIZE_T_EQUAL("limit_low",  RSARRAY_LIMIT_LOW_HARD,     rsarray_limit_low_get(array));
    TEST_SIZE_T_EQUAL("limit_high", RSARRAY_LIMIT_HIGH_DEFAULT, rsarray_limit_high_get(array));

    retcode = rsarray_limit_high_set(array, 0);
    TEST_INT32_EQUAL("limit_high", -1, retcode);

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

/*
 * Create an array with no elements (no storage allocated).
 */
#define NO_ELEMENTS     0
void test_rsarray_create_zero(void)
{
    rsarray_t *array = NULL;

    array = rsarray_create(NO_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size",       NO_ELEMENTS,                rsarray_size_get(array));
    TEST_SIZE_T_EQUAL("limit_low",  RSARRAY_LIMIT_LOW_HARD,     rsarray_limit_low_get(array));
    TEST_SIZE_T_EQUAL("limit_high", RSARRAY_LIMIT_HIGH_DEFAULT, rsarray_limit_high_get(array));

    TEST_PTR_EQUAL("poison", (void *)0x0, rsarray_poison_get(array));

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

/*
 * Create an array with no elements (no storage allocated) and show that we can
 * insert and delete elements.
 * Show that we can't delete back down to size zero.
 */
#define NO_ELEMENTS     0
void test_rsarray_zero_insert(void)
{
    rsarray_t *array = NULL;
    int32_t index;
    int32_t extra_1 = 1;
    int32_t extra_2 = 2;
    int32_t extra_3 = 3;
    int32_t expected_1[] = { 1 };
    int32_t expected_2[] = { 2, 1 };
    int32_t expected_3[] = { 3, 2, 1 };
    void *ptr;
    int32_t retcode;

    array = rsarray_create(NO_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size",       NO_ELEMENTS,                rsarray_size_get(array));
    TEST_SIZE_T_EQUAL("limit_low",  RSARRAY_LIMIT_LOW_HARD,     rsarray_limit_low_get(array));
    TEST_SIZE_T_EQUAL("limit_high", RSARRAY_LIMIT_HIGH_DEFAULT, rsarray_limit_high_get(array));

    TEST_PTR_EQUAL("poison", (void *)0x0, rsarray_poison_get(array));

    retcode = rsarray_element_insert(array, 0, &extra_1);
    TEST_INT32_EQUAL("rsarray_element_insert", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_1); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_1[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_insert(array, 0, &extra_2);
    TEST_INT32_EQUAL("rsarray_element_insert", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_2); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_2[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_insert(array, 0, &extra_3);
    TEST_INT32_EQUAL("rsarray_element_insert", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_3); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_3[index], *(int32_t *)ptr);
    }

    /*
     * Start deleting.
     */
    retcode = rsarray_element_delete(array, 0);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_2); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_2[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_delete(array, 0);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_1); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_1[index], *(int32_t *)ptr);
    }

    /*
     * Show that we can empty the array completely.
     */
    retcode = rsarray_element_delete(array, 0);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    TEST_SIZE_T_EQUAL("size", 0, rsarray_size_get(array));
    TEST_PTR_NULL("data", rsarray_data_get(array));

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

void test_rsarray_element_append(void)
{
    rsarray_t *array = NULL;
    int32_t numbers[] = { 0, 1, 2, 3, 4 };
    int32_t index;
    int32_t retcode;
    void *ptr;
    int32_t expected_0[] = { 0 };
    int32_t expected_1[] = { 0, 1 };
    int32_t expected_2[] = { 0, 1, 2 };
    int32_t expected_3[] = { 0, 1, 2, 3 };
    int32_t expected_4[] = { 0, 1, 2, 3, 4 };

    array = rsarray_create(NO_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size", NO_ELEMENTS, rsarray_size_get(array));

    /*
     * Append 0
     */
    retcode = rsarray_element_append(array, &numbers[0]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_0); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_0[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_append(array, &numbers[1]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_1); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_1[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_append(array, &numbers[2]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_2); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_2[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_append(array, &numbers[3]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_3); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_3[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_append(array, &numbers[4]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_4); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_4[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

void test_rsarray_element_prepend(void)
{
    rsarray_t *array = NULL;
    int32_t numbers[] = { 0, 1, 2, 3, 4 };
    int32_t index;
    int32_t retcode;
    void *ptr;
    int32_t expected_0[] = { 0 };
    int32_t expected_1[] = { 1, 0 };
    int32_t expected_2[] = { 2, 1, 0 };
    int32_t expected_3[] = { 3, 2, 1, 0 };
    int32_t expected_4[] = { 4, 3, 2, 1, 0 };

    array = rsarray_create(NO_ELEMENTS);
    TEST_PTR_NOT_NULL("rsarray_create", array);

    TEST_SIZE_T_EQUAL("size", NO_ELEMENTS, rsarray_size_get(array));

    /*
     * Append 0
     */
    retcode = rsarray_element_prepend(array, &numbers[0]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_0); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_0[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_prepend(array, &numbers[1]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_1); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_1[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_prepend(array, &numbers[2]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_2); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_2[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_prepend(array, &numbers[3]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_3); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_3[index], *(int32_t *)ptr);
    }

    retcode = rsarray_element_prepend(array, &numbers[4]);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    for (index = 0; index < NO_OF_ELEMENTS(expected_4); index++)
    {
        retcode = rsarray_element_get(array, index, &ptr);
        TEST_INT32_EQUAL("retcode", 0, retcode);
        TEST_INT32_EQUAL("number", expected_4[index], *(int32_t *)ptr);
    }

    rsarray_destroy(&array);
    TEST_PTR_NULL("rsarray_destroy", array);
}

