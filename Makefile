#
# Makefile for rsarray.
# Fri Dec 30 19:37:58 GMT 2022
#
# Copyright (C) 2022-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
# This file is part of rsarray.
#
# rsarray is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# rsarray is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with rsarray; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA
#
# Modification history:
# 2022-12-30 Extracted from csstring.
#

CC=gcc
CFLAGS=-fPIC -Wall -Werror -O
SHAREDFLAGS=-shared
CPPTESTFLAGS=-DTESTING
INCLUDES=-I/usr/local/include
LIBS=-lcutl -ltimber -lmal
INSTALL_DIR=/usr/local
INSTALL_LIB=$(INSTALL_DIR)/lib64
INSTALL_INC=$(INSTALL_DIR)/include

all: main test shared

test: rsarray_test
	./rsarray_test

main: rsarray

rsarray: rsarray.h rsarray.c
	$(CC) $(CFLAGS) -c rsarray.c

rsarray_test: rsarray_test.c rsarray.c rsarray.h
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o rsarray_testing.o -c rsarray.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -c rsarray_test.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o rsarray_test rsarray_test.o rsarray_testing.o $(LIBS)

shared: rsarray
	$(CC) $(SHAREDFLAGS) -o librsarray.so rsarray.o -ltimber -lmal

install:
	mkdir -p $(INSTALL_INC)
	cp rsarray.h $(INSTALL_INC)
	mkdir -p $(INSTALL_LIB)
	cp librsarray.so $(INSTALL_LIB)

uninstall:
	rm -f $(INSTALL_INC)/rsarray.h
	mkdir -p $(INSTALL_LIB)
	rm -f $(INSTALL_LIB)/librsarray.so

clean:
	rm -f *.o
	rm -f rsarray_test
	rm -f rsarray
	rm -f librsarray.so

