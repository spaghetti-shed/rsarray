/******************************************************************************
 * rsarray - Resizable array.
 *
 * Fri Dec 30 19:37:58 GMT 2022
 *
 * Copyright (C) 2022-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of rsarray.
 *
 * rsarray is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * rsarray is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with rsarray; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2022-12-30 Extracted from csstring.c.
 * 2022-12-31 Add rsarray_element_insert().
 * 2023-01-01 Add rsarray_element_delete().
 *            Add limits (low and high).
 * 2023-01-07 Automatically initialise the array with a poison value (or 0).
 * 2023-01-17 Allow initialisation with a size of 0.
 * 2023-01-18 Allow array to shrink to a size of 0.
 ******************************************************************************/

#include <string.h>
#include <stdint.h>
#include "timber.h"
#include "mal.h"
#include "rsarray.h"
#include "rsarray_private.h"

#if 0
/*
 * malloc() fails internally when we use this as our poison.
 */
#define RSARRAY_DEFAULT_POISON64  0xbaddcafedeadbeef
#else
#define RSARRAY_DEFAULT_POISON64  0xbaddbeefcafebadd
#endif
#define RSARRAY_DEFAULT_VALUE64   0x0000000000000000

rsarray_t *rsarray_create(size_t size)
{
    rsarray_t *array = NULL;

#if 0
    if (size < 1)
    {
    }
    else if (NULL == (array = MAL_MALLOC(sizeof(rsarray_t))))
    {
        TDR_DIAG_ERROR("Failed to allocate array structure.\n");
    }
    else if (NULL == (array->data = MAL_MALLOC(sizeof(void *) * size)))
    {
        TDR_DIAG_ERROR("Failed to allocate array data.\n");
    }
    else
    {
        array->size       = size;
        array->limit_low  = RSARRAY_LIMIT_LOW_HARD;
        array->limit_high = RSARRAY_LIMIT_HIGH_DEFAULT;
        array->poison     = (void *)RSARRAY_DEFAULT_VALUE64;

        if (0 != rsarray_clear(array))
        {
            TDR_DIAG_ERROR("Failed to clear array.\n");
        }
    }
#else
    if (NULL == (array = MAL_MALLOC(sizeof(rsarray_t))))
    {
        TDR_DIAG_ERROR("Failed to allocate array structure.\n");
    }
    else
    {
        if (0 == size)
        {
            array->data       = NULL;
            array->size       = size;
            array->limit_low  = RSARRAY_LIMIT_LOW_HARD;
            array->limit_high = RSARRAY_LIMIT_HIGH_DEFAULT;
            array->poison     = (void *)RSARRAY_DEFAULT_VALUE64;
        }
        else if (NULL == (array->data = MAL_MALLOC(sizeof(void *) * size)))
        {
            TDR_DIAG_ERROR("Failed to allocate array data.\n");
        }
        else
        {
            array->size       = size;
            array->limit_low  = RSARRAY_LIMIT_LOW_HARD;
            array->limit_high = RSARRAY_LIMIT_HIGH_DEFAULT;
            array->poison     = (void *)RSARRAY_DEFAULT_VALUE64;

            if (0 != rsarray_clear(array))
            {
                TDR_DIAG_ERROR("Failed to clear array.\n");
            }
        }
    }
#endif
    return array;
}

rsarray_t *rsarray_create_poisoned(size_t size)
{
    rsarray_t *array = NULL;

    if (size < 1)
    {
    }
    else if (NULL == (array = MAL_MALLOC(sizeof(rsarray_t))))
    {
        TDR_DIAG_ERROR("Failed to allocate array structure.\n");
    }
    else if (NULL == (array->data = MAL_MALLOC(sizeof(void *) * size)))
    {
        TDR_DIAG_ERROR("Failed to allocate array data.\n");
    }
    else
    {
        array->size       = size;
        array->limit_low  = RSARRAY_LIMIT_LOW_HARD;
        array->limit_high = RSARRAY_LIMIT_HIGH_DEFAULT;
        array->poison     = (void *)RSARRAY_DEFAULT_POISON64;

        if (0 != rsarray_clear(array))
        {
            TDR_DIAG_ERROR("Failed to clear array.\n");
        }
    }

    return array;
}

int32_t rsarray_clear(rsarray_t *array)
{
    int32_t retcode = -1;
    int32_t index;

    for (index = 0; index < array->size; index++)
    {
        array->data[index] = array->poison;
    }

    retcode = 0;

    return retcode;
}

void **rsarray_data_get(rsarray_t *array)
{
    return array->data;
}

size_t rsarray_size_get(rsarray_t *array)
{
    return array->size;
}

size_t rsarray_limit_low_get(rsarray_t *array)
{
    return array->limit_low;
}

int32_t rsarray_limit_low_set(rsarray_t *array, size_t limit_low)
{
    int32_t retcode = -1;

    if (limit_low > array->limit_high)
    {
        TDR_DIAG_ERROR("Low limit (%zd) too high (exceeds high limit) (%zd)).\n", limit_low, array->limit_high);
    }
    else
    {
        array->limit_low = limit_low;
        retcode = 0;
    }

    return retcode;
}

size_t rsarray_limit_high_get(rsarray_t *array)
{
    return array->limit_high;
}

int32_t rsarray_limit_high_set(rsarray_t *array, size_t limit_high)
{
    int32_t retcode = -1;

    if (0 == limit_high)
    {
        TDR_DIAG_ERROR("New high limit (%zd) not permitted.\n", limit_high);
    }
    else if (limit_high < array->size)
    {
        TDR_DIAG_ERROR("New high limit (%zd) lower than current size (%zd).\n", limit_high, array->size);
    }
    else
    {
        array->limit_high = limit_high;
        retcode = 0;
    }

    return retcode;
}

void *rsarray_poison_get(rsarray_t *array)
{
    return array->poison;
}

int32_t rsarray_poison_set(rsarray_t *array, void *poison)
{
    int32_t retcode = 0;

    array->poison = poison;

    return retcode;
}

void rsarray_destroy(rsarray_t **array)
{
    if (NULL == array)
    {
    }
    else if (NULL == *array)
    {
    }
    else
    {
        if (NULL != (*array)->data)
        {
            MAL_FREE((*array)->data);
        }

        MAL_FREE(*array);
        *array = NULL;
    }
}

int32_t rsarray_element_set(rsarray_t *array, int32_t index, void *ptr)
{
    int32_t retcode = -1;

    if (NULL == array)
    {
    }
    else if (index < 0)
    {
    }
    else if (index >= array->size)
    {
    }
    else
    {
        array->data[index] = ptr;
        retcode = 0;
    }

    return retcode;
}

int32_t rsarray_element_get(rsarray_t *array, int32_t index, void **ptr)
{
    int32_t retcode = -1;

    if (NULL == array)
    {
    }
    else if (index < 0)
    {
    }
    else if (index >= array->size)
    {
    }
    else if (NULL == ptr)
    {
    }
    else
    {
        *ptr = array->data[index];
        retcode = 0;
    }

    return retcode;
}

/*
 * rsarray_insert
 * rsarray_t *array: Resizable array in which to insert item (ptr).
 * index: Index in array (starting at 0) in which to insert ptr.
 * void *ptr: Pointer to insert in array.
 * returns:  Updated array with ptr inserted in array data as specified or -1 on failure.
 */
int32_t rsarray_element_insert(rsarray_t *array, int32_t index, void *ptr)
{
    int32_t retcode = -1;
    void **new_data;
    size_t span;

    if (NULL == array)
    {
    }
    else if (index < 0)
    {
    }
    else if (index > array->size)
    {
    }
    else if (array->size == array->limit_high)
    {
        TDR_DIAG_ERROR("High limit (%zd) reached. Insertion failed.\n", array->limit_high);
    }
    else if (NULL == (new_data = MAL_MALLOC(sizeof(void *) * (array->size + 1))))
    {
        TDR_DIAG_ERROR("Failed to allocate new array.\n");
    }
    else
    {
        /*
         * If index is 0, if the current array has zero size, memcpy will not
         * try to copy any data from the NULL pointer.
         */
        memcpy(new_data, array->data, index * sizeof(void *));
        memcpy(new_data + index, &ptr, sizeof(void *));
        span = (array->size - index) * sizeof(void *);
        memcpy(new_data + index + 1, array->data + index, span);
        if (NULL != array->data)
        {
            MAL_FREE(array->data);
        }
        array->data = new_data;
        array->size += 1;
        retcode = 0;
    }

    return retcode;
}

int32_t rsarray_element_delete(rsarray_t *array, int32_t index)
{
    int32_t retcode = -1;
    void **new_data;
    size_t span;
    size_t new_size;

    if (NULL == array)
    {
    }
    else if (index < 0)
    {
    }
    else if (index > array->size)
    {
    }
    else if (array->size == array->limit_low)
    {
        TDR_DIAG_ERROR("Element will not be deleted. Low limit (%zd) reached.\n", array->limit_low);
    }
    else
    {
        new_size = array->size - 1;

        if (0 == new_size)
        {
            MAL_FREE(array->data);
            array->data = NULL;
            array->size = 0;
            retcode = 0;
        }
        else if (NULL == (new_data = MAL_MALLOC(sizeof(void *) * (array->size - 1))))
        {
            TDR_DIAG_ERROR("Failed to allocate new array.\n");
        }
        else
        {
            memcpy(new_data, array->data, index * sizeof(void *));
            span = (array->size - index) * sizeof(void *);
            memcpy(new_data + index, array->data + index + 1, span);
            MAL_FREE(array->data);
            array->data = new_data;
            array->size -= 1;
            retcode = 0;
        }
    }

    return retcode;
}

int32_t rsarray_element_append(rsarray_t *array, void *ptr)
{
    return rsarray_element_insert(array, rsarray_size_get(array), ptr);
}

int32_t rsarray_element_prepend(rsarray_t *array, void *ptr)
{
    return rsarray_element_insert(array, 0, ptr);
}

