/******************************************************************************
 * rsarray_private (header) - Resizable array private interface (header).
 *
 * Wed Jan 18 19:54:24 GMT 2023
 *
 * Copyright (C) 2022-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of rsarray.
 *
 * rsarray is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * rsarray is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with rsarray; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2023-01-18 Extracted from rsarray.h.
 ******************************************************************************/

#ifndef __RSARRAY_PRIVATE_H__
#define __RSARRAY_PRIVATE_H__

#include <stdarg.h>

#define RSARRAY_LIMIT_LOW_HARD      0
#define RSARRAY_LIMIT_HIGH_DEFAULT  1024
#define RSARRAY_SIZE_DEFAULT        16

#ifdef __cplusplus
extern "C"
{
#endif

void **rsarray_data_get(rsarray_t *array);


#ifdef __cplusplus
}
#endif

#endif /* __RSARRAY_PRIVATE_H__ */

