/******************************************************************************
 * rsarray (header) - Resizable array (header).
 *
 * Fri Dec 30 19:37:58 GMT 2022
 *
 * Copyright (C) 2022-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of rsarray.
 *
 * rsarray is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * rsarray is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with rsarray; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2022-12-30 Extracted from csstring.h.
 * 2022-12-31 Add rsarray_element_insert().
 * 2023-01-01 Add rsarray_element_delete().
 *            Add limits (low and high).
 * 2023-01-07 Automatically initialise the array with a poison value (or 0).
 * 2023-01-21 Append and prepend.
 ******************************************************************************/

#ifndef __RSARRAY_H__
#define __RSARRAY_H__

#include <stdarg.h>

#define RSARRAY_LIMIT_LOW_HARD      0
#define RSARRAY_LIMIT_HIGH_DEFAULT  1024
#define RSARRAY_SIZE_DEFAULT        16

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct
{
    void **data;
    size_t size;
    size_t limit_low;
    size_t limit_high;
    void *poison;
}
rsarray_t;

rsarray_t *rsarray_create(size_t size);
rsarray_t *rsarray_create_poisoned(size_t size);

int32_t rsarray_clear(rsarray_t *array);

size_t rsarray_size_get(rsarray_t *array);
size_t rsarray_limit_low_get(rsarray_t *array);
int32_t rsarray_limit_low_set(rsarray_t *array, size_t limit_low);
size_t rsarray_limit_high_get(rsarray_t *array);
int32_t rsarray_limit_high_set(rsarray_t *array, size_t limit_high);
void *rsarray_poison_get(rsarray_t *array);
int32_t rsarray_poison_set(rsarray_t *array, void *poison);


void rsarray_destroy(rsarray_t **array);

int32_t rsarray_element_set(rsarray_t *array, int32_t index, void *ptr);
int32_t rsarray_element_get(rsarray_t *array, int32_t index, void **ptr);


/*
 * rsarray_insert
 * rsarray_t *array: Resizable array in which to insert item (ptr).
 * index: Index in array (starting at 0) in which to insert ptr.
 * void *ptr: Pointer to insert in array.
 * returns:  Updated array with ptr inserted in array data as specified or -1 on failure.
 */
int32_t rsarray_element_insert(rsarray_t *array, int32_t index, void *ptr);

int32_t rsarray_element_delete(rsarray_t *array, int32_t index);

int32_t rsarray_element_append(rsarray_t *array, void *ptr);

int32_t rsarray_element_prepend(rsarray_t *array, void *ptr);

#ifdef __cplusplus
}
#endif

#endif /* __RSARRAY_H__ */

